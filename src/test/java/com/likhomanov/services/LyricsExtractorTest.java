package com.likhomanov.services;

import com.likhomanov.exceptions.LyricsExtractorException;
import com.likhomanov.model.Lyrics;
import com.likhomanov.services.webpageparsers.AltwallWebPageParser;
import com.likhomanov.services.webpageparsers.TekstyPesenokWebPageParser;
import com.likhomanov.services.webpageparsers.WebPageParser;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LyricsExtractorTest {

    @Mock
    private Document webPageMock;
    @Mock
    private AltwallWebPageParser altwallParserMock;
    @Mock
    private TekstyPesenokWebPageParser tekstyPesenokParserMock;
    @Spy
    private ArrayList<WebPageParser> webPageParsers;
    @InjectMocks
    private LyricsExtractor lyricsExtractor;

    @BeforeEach
    public void init() {
        if (webPageParsers.isEmpty()) {
            webPageParsers.add(altwallParserMock);
            webPageParsers.add(tekstyPesenokParserMock);
        }
    }

    @Test
    public void lyricsExtractorExceptionWillBeThrownWhenWebPageIsNull() {
        assertThrows(LyricsExtractorException.class, () -> lyricsExtractor.extractLyrics(null),
                     "Null reference has been accepted as a parameter");
    }

    @Test
    public void listOfLyricsWillBeReturnedWhenGivenWebPageOfAltwallNet() {
        doReturn("altwall.net").when(altwallParserMock).getTargetUrl();
        doReturn("altwall.net").when(webPageMock).location();

        List<Lyrics> lyrics = lyricsExtractor.extractLyrics(webPageMock);

        assertNotNull(lyrics,"List of lyrics is null");

        verify(altwallParserMock).getTargetUrl();
        verify(altwallParserMock).parseWebPage(webPageMock);
        verify(webPageMock).location();
    }

    @Test
    public void listOfLyricsWillBeReturnedWhenGivenWebPageOfTekstyPesenokRu() {
        doReturn("altwall.net").when(altwallParserMock).getTargetUrl();
        doReturn("teksty-pesenok.ru").when(tekstyPesenokParserMock).getTargetUrl();
        doReturn("teksty-pesenok.ru").when(webPageMock).location();

        List<Lyrics> lyrics = lyricsExtractor.extractLyrics(webPageMock);

        assertNotNull(lyrics,"List of lyrics is null");

        verify(altwallParserMock).getTargetUrl();
        verifyNoMoreInteractions(altwallParserMock);
        verify(tekstyPesenokParserMock).getTargetUrl();
        verify(tekstyPesenokParserMock).parseWebPage(webPageMock);
        verify(webPageMock, times(2)).location();
    }

    @Test
    public void lyricsExtractorExceptionWillBeThrownWhenGivenWebPageOfUnsupportedWbSite() {
        doReturn("altwall.net").when(altwallParserMock).getTargetUrl();
        doReturn("teksty-pesenok.ru").when(tekstyPesenokParserMock).getTargetUrl();
        doReturn("unknown.com").when(webPageMock).location();

        assertThrows(LyricsExtractorException.class, () -> lyricsExtractor.extractLyrics(webPageMock),
                     "Unsupported web site has been accepted");

        verify(altwallParserMock).getTargetUrl();
        verifyNoMoreInteractions(altwallParserMock);
        verify(tekstyPesenokParserMock).getTargetUrl();
        verifyNoMoreInteractions(tekstyPesenokParserMock);
        verify(webPageMock, times(2)).location();
    }
}