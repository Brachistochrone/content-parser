package com.likhomanov.services;

import com.likhomanov.model.Lyrics;
import com.likhomanov.model.Word;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class LyricsWordsCounterTest {

    @Mock
    private Lyrics lyrics;
    @InjectMocks
    private LyricsWordsCounter wordsCounter;

    @Test
    public void mapOfWordsAndTheirNumbersWillBeReturnedWhenGivenLyrics() {
        doReturn(Arrays.asList("First! Line,", "Second? line", "Third - LINE...")).when(lyrics).getLines();

        Map<String, Word> wordsMap = wordsCounter.countWords(lyrics);

        assertNotNull(wordsMap, "Map of words is null");
        assertFalse(wordsMap.isEmpty(), "Map of words is empty");
        assertFalse(wordsMap.containsKey(""), "Map of words contains empty string");
        assertFalse(wordsMap.containsKey(" "), "Map of words contains white space");
        assertFalse(wordsMap.containsKey("."), "Map of words contains punctuation marks");
        assertFalse(wordsMap.containsKey(","), "Map of words contains punctuation marks");
        assertFalse(wordsMap.containsKey("!"), "Map of words contains punctuation marks");
        assertFalse(wordsMap.containsKey("?"), "Map of words contains punctuation marks");
        assertFalse(wordsMap.containsKey("-"), "Map of words contains punctuation marks");
        assertTrue(wordsMap.containsKey("first"), "Word has not been extracted correctly or contains capital letters");
        assertTrue(wordsMap.containsKey("second"), "Word has not been extracted correctly or contains capital letters");
        assertTrue(wordsMap.containsKey("third"), "Word has not been extracted correctly or contains capital letters");
        assertTrue(wordsMap.containsKey("line"), "Word has not been extracted correctly or contains capital letters");
        assertEquals(1, wordsMap.get("first").getQuantity(), "Number of words has been counted wrongly");
        assertEquals(1, wordsMap.get("second").getQuantity(), "Number of words has been counted wrongly");
        assertEquals(1, wordsMap.get("third").getQuantity(), "Number of words has been counted wrongly");
        assertEquals(3, wordsMap.get("line").getQuantity(), "Number of words has been counted wrongly");

        verify(lyrics).getLines();
    }
}