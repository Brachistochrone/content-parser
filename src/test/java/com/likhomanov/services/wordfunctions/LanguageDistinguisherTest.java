package com.likhomanov.services.wordfunctions;

import com.likhomanov.exceptions.WordFunctionException;
import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static com.likhomanov.enums.Language.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class LanguageDistinguisherTest {

    private final Map<String, Word> words = new HashMap<>();
    private final LyricsStatisticsInstance statistics = new LyricsStatisticsInstance();
    @Mock
    private KeyWordCounter keyWordCounterMock;
    @InjectMocks
    private LanguageDistinguisher languageDistinguisher;

    @BeforeEach
    public void init() {
        words.clear();
        words.put("str1", new Word("str1"));
        words.put("str2", new Word("str2"));
        words.put("str3", new Word("str3"));
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenWordMapIsNull() {
        assertThrows(WordFunctionException.class,
                     () -> languageDistinguisher.apply(null, statistics),
                     "Null reference has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenLyricsStatisticsIsNull() {
        assertThrows(WordFunctionException.class,
                     () -> languageDistinguisher.apply(words,null),
                     "Null reference has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenWordMapIsEmpty() {
        assertThrows(WordFunctionException.class,
                     () -> languageDistinguisher.apply(new HashMap<>(), statistics),
                     "Empty map has been accepted as a parameter");
    }

    @Test
    public void sameInstanceOfLyricsStatisticsWillBeReturned() {
        LyricsStatisticsInstance returnedStatistics = languageDistinguisher.apply(words, statistics);

        assertSame(statistics, returnedStatistics, "Method returned different object of LyricsStatistics");
    }

    @Test
    public void englishLanguageWillBeDistinguished() {
        doReturn(20).when(keyWordCounterMock).count(words, languageDistinguisher.getEnglishKeyWords());

        LyricsStatisticsInstance statistics = languageDistinguisher.apply(words, this.statistics);

        assertEquals(ENGLISH, statistics.getLanguage(),"English language has not been distinguished");

        verify(keyWordCounterMock).count(words, languageDistinguisher.getEnglishKeyWords());
    }

    @Test
    public void russianLanguageWillBeDistinguished() {
        doReturn(0).when(keyWordCounterMock).count(words, languageDistinguisher.getEnglishKeyWords());
        doReturn(20).when(keyWordCounterMock).count(words, languageDistinguisher.getRussianKeyWords());

        LyricsStatisticsInstance statistics = languageDistinguisher.apply(words, this.statistics);

        assertEquals(RUSSIAN, statistics.getLanguage(),"Russian language has not been distinguished");

        verify(keyWordCounterMock).count(words, languageDistinguisher.getEnglishKeyWords());
        verify(keyWordCounterMock).count(words, languageDistinguisher.getRussianKeyWords());
    }

    @Test
    public void otherLanguageWillBeDistinguished() {
        doReturn(0).when(keyWordCounterMock).count(words, languageDistinguisher.getRussianKeyWords());
        doReturn(0).when(keyWordCounterMock).count(words, languageDistinguisher.getEnglishKeyWords());

        LyricsStatisticsInstance statistics = languageDistinguisher.apply(words, this.statistics);

        assertEquals(OTHER, statistics.getLanguage(),"Other language has not been distinguished");

        verify(keyWordCounterMock).count(words, languageDistinguisher.getRussianKeyWords());
        verify(keyWordCounterMock).count(words, languageDistinguisher.getEnglishKeyWords());
    }
}