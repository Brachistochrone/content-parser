package com.likhomanov.services.wordfunctions;

import com.likhomanov.exceptions.WordFunctionException;
import com.likhomanov.model.Word;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class KeyWordCounterTest {

    private final Map<String, Word> words = new HashMap<>();
    private final KeyWordCounter wordCounter = new KeyWordCounter();
    private final LanguageDistinguisher languageDistinguisher = new LanguageDistinguisher(wordCounter);

    @BeforeEach
    public void init() {
        words.clear();

        words.put("the", new Word("the"));
        words.put("str2", new Word("str2"));
        words.put("and", new Word("and"));
        words.put("str4", new Word("str4"));
        words.put("to", new Word("to"));

        words.get("the").count();
        words.get("and").count();
        words.get("and").count();
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenListIsNull() {
        assertThrows(WordFunctionException.class,
                     () -> wordCounter.count(words,null),
                     "Null reference has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenListIsEmpty() {
        assertThrows(WordFunctionException.class,
                     () -> wordCounter.count(words, new ArrayList<>()),
                     "Empty list has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenListContainsEmptyStrings() {
        assertThrows(WordFunctionException.class,
                     () -> wordCounter.count(words, Arrays.asList("Hi", "", "there")),
                     "List with empty strings has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenListContainsWhiteSpaces() {
        assertThrows(WordFunctionException.class,
                     () -> wordCounter.count(words, Arrays.asList("Hi", " ", "there")),
                     "List with white spaces has been accepted as a parameter");
    }

    @Test
    public void numberOfKeywordsOccurrencesWillBeCounted() {
        assertEquals(6, wordCounter.count(words, languageDistinguisher.getEnglishKeyWords()),
                     "Number of keyword occurrences has been counted wrongly");
    }
}