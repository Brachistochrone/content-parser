package com.likhomanov.services.wordfunctions;

import com.likhomanov.exceptions.WordFunctionException;
import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class AllWordsCounterTest {

    private final WordFunction wordFunction = new AllWordsCounter();
    private final Map<String, Word> words = new HashMap<>();
    private final LyricsStatisticsInstance statistics = new LyricsStatisticsInstance();

    @BeforeEach
    public void init() {
        words.clear();
        words.put("str1", new Word("str1"));
        words.put("str2", new Word("str2"));
        words.put("str3", new Word("str3"));
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenWordMapIsNull() {
        assertThrows(WordFunctionException.class,
                     () -> wordFunction.apply(null, statistics),
                     "Null reference has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenLyricsStatisticsIsNull() {
        assertThrows(WordFunctionException.class,
                     () -> wordFunction.apply(words,null),
                     "Null reference has been accepted as a parameter");
    }

    @Test
    public void wordFunctionExceptionWillBeThrownWhenWordMapIsEmpty() {
        assertThrows(WordFunctionException.class,
                     () -> wordFunction.apply(new HashMap<>(), statistics),
                     "Empty map has been accepted as a parameter");
    }

    @Test
    public void sameInstanceOfLyricsStatisticsWillBeReturned() {
        LyricsStatisticsInstance returnedStatistics = wordFunction.apply(words, statistics);

        assertSame(statistics, returnedStatistics, "Method returned different object of LyricsStatistics");
    }

    @Test
    public void numberOfAllWordsWillBeCounted() {
        assertEquals(3, wordFunction.apply(words, statistics).getTotalNumberOfWords(),
                     "Total number of words has been counted wrongly");
    }
}