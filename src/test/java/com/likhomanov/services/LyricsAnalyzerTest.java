package com.likhomanov.services;

import com.likhomanov.exceptions.LyricsAnalyzerException;
import com.likhomanov.model.Lyrics;
import com.likhomanov.model.LyricsStatistics;
import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import com.likhomanov.services.wordfunctions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LyricsAnalyzerTest {

    @Captor
    private ArgumentCaptor<Map<String, Word>> mapCaptor;
    @Captor
    private ArgumentCaptor<LyricsStatisticsInstance> statisticsCaptor;
    @Spy
    private ArrayList<WordFunction> wordFunctions;
    @Mock
    private AllWordsCounter allWordsCounter;
    @Mock
    private LanguageDistinguisher languageDistinguisher;
    @Mock
    private MostPopularWordFinder mostPopularWordFinder;
    @Mock
    private SameWordsCounter sameWordsCounter;
    @Mock
    private UniqueWordsCounter uniqueWordsCounter;
    @Mock
    private LyricsWordsCounter wordsCounter;
    private List<Lyrics> lyricsList;
    @Mock
    private Lyrics originalLyrics;
    @Mock
    private Lyrics translatedLyrics;
    @Mock
    Map<String, Word> originalWordsMap;
    @Mock
    Map<String, Word> translatedWordsMap;
    @InjectMocks
    private LyricsAnalyzer lyricsAnalyzer;

    @BeforeEach
    public void init() {
        if (wordFunctions.isEmpty()) {
            wordFunctions.add(allWordsCounter);
            wordFunctions.add(languageDistinguisher);
            wordFunctions.add(mostPopularWordFinder);
            wordFunctions.add(sameWordsCounter);
            wordFunctions.add(uniqueWordsCounter);
        }

        if (lyricsList == null) {
            lyricsList = Arrays.asList(originalLyrics, translatedLyrics);
        }
    }

    @Test
    public void lyricsAnalyzerExceptionWillBeThrownWhenListOfLyricsIsNull() {
        assertThrows(LyricsAnalyzerException.class, () -> lyricsAnalyzer.analyze(null),
                     "Null has been accepted as a parameter");
    }

    @Test
    public void lyricsAnalyzerExceptionWillBeThrownWhenListOfLyricsIsEmpty() {
        assertThrows(LyricsAnalyzerException.class, () -> lyricsAnalyzer.analyze(new ArrayList<>()),
                     "Empty list of lyrics has been accepted as a parameter");
    }

    @Test
    public void listOfLyricsStatisticsWillBeReturnedWhenGivenListOfLyrics() {
        doReturn(originalWordsMap).when(wordsCounter).countWords(originalLyrics);
        doReturn(translatedWordsMap).when(wordsCounter).countWords(translatedLyrics);
        doReturn("somesongwebsite.com").when(originalLyrics).getUrl();
        doReturn("somesongwebsite.com").when(translatedLyrics).getUrl();
        doReturn(Arrays.asList("This is", "original", "lyrics")).when(originalLyrics).getLines();
        doReturn(Arrays.asList("This is", "translated", "lyrics")).when(translatedLyrics).getLines();

        List<LyricsStatistics> statisticsList = lyricsAnalyzer.analyze(lyricsList);
        LyricsStatistics originalStatistics = statisticsList.get(0);
        LyricsStatistics translatedStatistics = statisticsList.get(1);

        assertNotNull(statisticsList, "List of statistics is null");
        assertEquals(2, statisticsList.size(),
                     "List of statistics either is empty or does not contain two objects");
        assertNotNull(originalStatistics.getDate(),
                      "Statistics does not contain date");
        assertNotNull(translatedStatistics.getDate(),
                      "Statistics does not contain date");
        assertNotNull(originalStatistics.getTimestamp(),
                      "Statistics does not contain timestamp");
        assertNotNull(translatedStatistics.getTimestamp(),
                      "Statistics does not contain timestamp");
        assertEquals("somesongwebsite.com", originalStatistics.getUrl(),
                     "Statistics contains wrong URL");
        assertEquals("somesongwebsite.com", translatedStatistics.getUrl(),
                     "Statistics contains wrong URL");
        assertEquals("This is\noriginal\nlyrics", originalStatistics.getLyrics(),
                     "Statistics contains wrong lyrics");
        assertEquals("This is\ntranslated\nlyrics", translatedStatistics.getLyrics(),
                     "Statistics contains wrong lyrics");
        verify(wordsCounter).countWords(originalLyrics);
        verify(wordsCounter).countWords(translatedLyrics);
        verify(originalLyrics).getUrl();
        verify(translatedLyrics).getUrl();
        verify(originalLyrics).getLines();
        verify(translatedLyrics).getLines();
        verify(allWordsCounter, times(2)).apply(mapCaptor.capture(), statisticsCaptor.capture());
        assertSame(originalWordsMap, mapCaptor.getAllValues().get(0));
        assertSame(translatedWordsMap, mapCaptor.getAllValues().get(1));
        assertEquals("This is\noriginal\nlyrics", statisticsCaptor.getAllValues().get(0).getLyrics());
        assertEquals("This is\ntranslated\nlyrics", statisticsCaptor.getAllValues().get(1).getLyrics());
        verify(languageDistinguisher, times(2)).apply(mapCaptor.capture(), statisticsCaptor.capture());
        assertSame(originalWordsMap, mapCaptor.getAllValues().get(2));
        assertSame(translatedWordsMap, mapCaptor.getAllValues().get(3));
        assertEquals("This is\noriginal\nlyrics", statisticsCaptor.getAllValues().get(2).getLyrics());
        assertEquals("This is\ntranslated\nlyrics", statisticsCaptor.getAllValues().get(3).getLyrics());
        verify(mostPopularWordFinder, times(2)).apply(mapCaptor.capture(), statisticsCaptor.capture());
        assertSame(originalWordsMap, mapCaptor.getAllValues().get(4));
        assertSame(translatedWordsMap, mapCaptor.getAllValues().get(5));
        assertEquals("This is\noriginal\nlyrics", statisticsCaptor.getAllValues().get(4).getLyrics());
        assertEquals("This is\ntranslated\nlyrics", statisticsCaptor.getAllValues().get(5).getLyrics());
        verify(sameWordsCounter, times(2)).apply(mapCaptor.capture(), statisticsCaptor.capture());
        assertSame(originalWordsMap, mapCaptor.getAllValues().get(6));
        assertSame(translatedWordsMap, mapCaptor.getAllValues().get(7));
        assertEquals("This is\noriginal\nlyrics", statisticsCaptor.getAllValues().get(6).getLyrics());
        assertEquals("This is\ntranslated\nlyrics", statisticsCaptor.getAllValues().get(7).getLyrics());
        verify(uniqueWordsCounter, times(2)).apply(mapCaptor.capture(), statisticsCaptor.capture());
        assertSame(originalWordsMap, mapCaptor.getAllValues().get(8));
        assertSame(translatedWordsMap, mapCaptor.getAllValues().get(9));
        assertEquals("This is\noriginal\nlyrics", statisticsCaptor.getAllValues().get(8).getLyrics());
        assertEquals("This is\ntranslated\nlyrics", statisticsCaptor.getAllValues().get(9).getLyrics());
    }
}