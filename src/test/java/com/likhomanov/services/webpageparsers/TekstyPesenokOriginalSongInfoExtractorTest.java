package com.likhomanov.services.webpageparsers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class TekstyPesenokOriginalSongInfoExtractorTest {

    @InjectMocks
    private TekstyPesenokOriginalSongInfoExtractor songInfoExtractor;

    @Test
    public void listOfSongLinesWillBeReturnedWithoutEmptyLinesSingleAndDoubleSpacesAndWebsiteUrl() {
        List<String> lines = songInfoExtractor.filterLines("First line\n\n" +
                                                                 "Second line\n \n" +
                                                                 "Third line\n  \n" +
                                                                 "teksty-pesenok.ru\n \n" +
                                                                 "Fifth line");

        assertNotNull(lines, "List of lines is null");
        assertFalse(lines.isEmpty(), "List of lines is empty");
        assertFalse(lines.contains(""), "List of lines contains empty lines");
        assertFalse(lines.contains(" "), "List of lines contains single white spaces");
        assertFalse(lines.contains("  "), "List of lines contains double white spaces");
        assertFalse(lines.contains("teksty-pesenok.ru"), "List of lines contains website URL");
    }
}