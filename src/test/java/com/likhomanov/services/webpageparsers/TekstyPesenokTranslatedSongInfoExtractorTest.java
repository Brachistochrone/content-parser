package com.likhomanov.services.webpageparsers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TekstyPesenokTranslatedSongInfoExtractorTest {

    @Mock
    private Document webPageMock;
    @Mock
    private Elements contentMock;
    @Mock
    private Element firstElementMock;
    @Mock
    private Element secondElementMock;
    @Mock
    private Element thirdElementMock;
    @Mock
    private Element forthElementMock;
    @InjectMocks
    private TekstyPesenokTranslatedSongInfoExtractor songInfoExtractor;

    @Test
    public void listOfSongLinesWillBeReturnedUrlLineWillBeRemovedWhenGivenWebPageAndTag() {
        doReturn(contentMock)
                .when(webPageMock)
                .getElementsByClass("tag");
        doReturn(false)
                .when(contentMock)
                .isEmpty();
        doAnswer(invocationOnMock -> Stream.of(firstElementMock, secondElementMock, thirdElementMock, forthElementMock))
                .when(contentMock)
                .stream();
        doReturn("First line")
                .when(firstElementMock)
                .text();
        doReturn("Second line")
                .when(secondElementMock)
                .text();
        doReturn("Источник teksty-pesenok.ru")
                .when(thirdElementMock)
                .text();
        doReturn("Forth line")
                .when(forthElementMock)
                .text();

        List<String> lines = songInfoExtractor.extractLyrics(webPageMock, "tag");

        assertNotNull(lines, "List of song lines is null");
        assertFalse(lines.isEmpty(), "List of song lines is empty");
        assertFalse(lines.contains("Источник teksty-pesenok.ru"), "List of song lines contains URL od website");

        verify(webPageMock).getElementsByClass("tag");
        verify(contentMock).isEmpty();
        verify(contentMock).stream();
        verify(firstElementMock).text();
        verify(secondElementMock).text();
        verify(thirdElementMock).text();
        verify(forthElementMock).text();
    }
}