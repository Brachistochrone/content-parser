package com.likhomanov.services.webpageparsers;

import com.likhomanov.exceptions.LyricsExtractorException;
import com.likhomanov.model.Lyrics;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TekstyPesenokWebPageParserTest {

    @Mock
    private Document webPageMock;
    @Mock
    private TekstyPesenokOriginalSongInfoExtractor originalInfoExtractorMock;
    @Mock
    private TekstyPesenokTranslatedSongInfoExtractor translatedInfoExtractorMock;
    private TekstyPesenokWebPageParser webPageParser;

    @BeforeEach
    public void init() {
        webPageParser = new TekstyPesenokWebPageParser(originalInfoExtractorMock,
                                                       translatedInfoExtractorMock);
    }

    @Test
    public void targetUrlWillBeReturned() {
        assertEquals("teksty-pesenok.ru", webPageParser.getTargetUrl(),
                     "Target URL does not match");
    }

    @Test
    public void listOfLyricsWillBeReturnedWhenGivenWebPageWithTwoSongs() {
        doReturn(new Element("tag"))
                .when(webPageMock)
                .selectFirst("span.songTextTrans.songText");
        doReturn("teksty-pesenok.ru/some_song")
                .when(webPageMock)
                .location();
        doReturn(Collections.singletonList("First original line"))
                .when(translatedInfoExtractorMock)
                .extractLyrics(webPageMock, "songTextTrans songText");
        doReturn(Collections.singletonList("First translated line"))
                .when(translatedInfoExtractorMock)
                .extractLyrics(webPageMock, "songTextTrans songTrans");
        doReturn(Arrays.asList("Original Song", "Translated Song"))
                .when(translatedInfoExtractorMock)
                .extractTitle(webPageMock, "h1");

        List<Lyrics> listOfLyrics = webPageParser.parseWebPage(webPageMock);
        Lyrics originalLyrics = listOfLyrics.get(0);
        Lyrics translatedLyrics = listOfLyrics.get(1);
        String urlOfOriginalLyrics = originalLyrics.getUrl();
        String urlOfTranslatedLyrics = translatedLyrics.getUrl();
        String titleOfOriginalLyrics = originalLyrics.getTitle();
        String titleOfTranslatedLyrics = translatedLyrics.getTitle();
        List<String> linesOfOriginalLyrics = originalLyrics.getLines();
        List<String> linesOfTranslatedLyrics = translatedLyrics.getLines();

        assertNotNull(listOfLyrics, "List of lyrics is null");
        assertEquals(2, listOfLyrics.size(), "List of lyrics does not contain two lyrics");
        assertEquals("teksty-pesenok.ru/some_song", urlOfOriginalLyrics, "Lyrics URL does not match");
        assertEquals("teksty-pesenok.ru/some_song", urlOfTranslatedLyrics, "Lyrics URL does not match");
        assertEquals("Original Song", titleOfOriginalLyrics, "Original title does not match");
        assertEquals("Translated Song", titleOfTranslatedLyrics, "Translated title does not match");
        assertEquals("First original line", linesOfOriginalLyrics.get(0), "Original lines does not match");
        assertEquals("First translated line", linesOfTranslatedLyrics.get(0), "Translated lines does not match");

        verify(webPageMock).selectFirst("span.songTextTrans.songText");
        verify(webPageMock, times(2)).location();
        verify(translatedInfoExtractorMock).extractLyrics(webPageMock, "songTextTrans songText");
        verify(translatedInfoExtractorMock).extractLyrics(webPageMock, "songTextTrans songTrans");
        verify(translatedInfoExtractorMock).extractTitle(webPageMock, "h1");
    }

    @Test
    public void listOfLyricsWillBeReturnedWhenGivenWebPageWithOneSong() {
        doReturn(null)
                .when(webPageMock)
                .selectFirst("span.songTextTrans.songText");
        doReturn(new Element("tag"))
                .when(webPageMock)
                .selectFirst("div.textPesni");
        doReturn("teksty-pesenok.ru/some_song")
                .when(webPageMock)
                .location();
        doReturn(Collections.singletonList("First original line"))
                .when(originalInfoExtractorMock)
                .extractLyrics(webPageMock, "textPesni");
        doReturn(Collections.singletonList("Original Song"))
                .when(originalInfoExtractorMock)
                .extractTitle(webPageMock, "h1");

        List<Lyrics> listOfLyrics = webPageParser.parseWebPage(webPageMock);
        String url = listOfLyrics.get(0).getUrl();
        String title = listOfLyrics.get(0).getTitle();
        List<String> lines = listOfLyrics.get(0).getLines();

        assertNotNull(listOfLyrics, "List of lyrics is null");
        assertEquals(1, listOfLyrics.size(), "List of lyrics contains more or less than one lyrics");
        assertEquals("teksty-pesenok.ru/some_song", url, "Lyrics URL does not match");
        assertEquals("Original Song", title, "Lyrics title does not match");
        assertEquals("First original line", lines.get(0), "Lyrics lines does not match");

        verify(webPageMock).selectFirst("span.songTextTrans.songText");
        verify(webPageMock).selectFirst("div.textPesni");
        verify(webPageMock).location();
        verify(originalInfoExtractorMock).extractLyrics(webPageMock, "textPesni");
        verify(originalInfoExtractorMock).extractTitle(webPageMock, "h1");
    }

    @Test
    public void lyricsExtractorExceptionWillBeThrownWhenGivenUnsupportedVersionOfWebPage() {
        doReturn(null)
                .when(webPageMock)
                .selectFirst("span.songTextTrans.songText");
        doReturn(null)
                .when(webPageMock)
                .selectFirst("div.textPesni");

        assertThrows(LyricsExtractorException.class, () -> webPageParser.parseWebPage(webPageMock),
                     "LyricsExtractorException has not been thrown");

        verify(webPageMock).selectFirst("span.songTextTrans.songText");
        verify(webPageMock).selectFirst("div.textPesni");
    }
}