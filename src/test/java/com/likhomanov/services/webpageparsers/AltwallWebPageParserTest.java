package com.likhomanov.services.webpageparsers;

import com.likhomanov.model.Lyrics;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AltwallWebPageParserTest {

    @Mock
    private Document webPageMock;
    @Mock
    private AltwallSongInfoExtractor infoExtractorMock;
    @InjectMocks
    private AltwallWebPageParser webPageParser;

    @Test
    public void targetUrlWillBeReturned() {
        assertEquals("altwall.net", webPageParser.getTargetUrl(),
                     "Target URL does not match");
    }

    @Test
    public void listOfLyricsWillBeReturnedWhenGivenWebPage() {
        doReturn("altwall.net/some_song").when(webPageMock).location();
        doReturn(Collections.singletonList("First line")).when(infoExtractorMock).extractLyrics(webPageMock, "article");
        doReturn(Collections.singletonList("Some song")).when(infoExtractorMock).extractTitle(webPageMock, "h1");

        List<Lyrics> lyrics = webPageParser.parseWebPage(webPageMock);
        String url = lyrics.get(0).getUrl();
        String title = lyrics.get(0).getTitle();
        List<String> lines = lyrics.get(0).getLines();

        assertNotNull(lyrics, "List of lyrics is null");
        assertNotNull(lyrics.get(0), "List of lyrics is empty");
        assertEquals("altwall.net/some_song", url, "Lyrics URL does not match");
        assertEquals("Some song", title, "Lyrics title does not match");
        assertEquals("First line", lines.get(0), "Lyrics first line does not match");

        verify(infoExtractorMock).extractLyrics(webPageMock, "article");
        verify(infoExtractorMock).extractTitle(webPageMock, "h1");
        verify(webPageMock).location();
    }
}