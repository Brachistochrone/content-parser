package com.likhomanov.services.webpageparsers;

import com.likhomanov.exceptions.LyricsExtractorException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WebPageSongInfoExtractorTest {

    @Mock
    private Document webPageMock;
    @Mock
    private Elements elementsMock;
    @Mock
    private Element firstElementMock;
    @Mock
    private Element secondElementMock;
    @InjectMocks
    private WebPageSongInfoExtractor songInfoExtractor;

    @Test
    public void lyricsExtractorExceptionWillBeThrownWhenGivenNullWebPage() {
        assertThrows(LyricsExtractorException.class,
                     () -> songInfoExtractor.validateContent(null, "tag"),
                     "Null has been accepted as a parameter");
    }

    @Test
    public void lyricsExtractorExceptionWillBeThrownWhenGivenEmptyWebPage() {
        assertThrows(LyricsExtractorException.class,
                     () -> songInfoExtractor.validateContent(new Elements(), "tag"),
                     "Empty web page has been accepted as a parameter");
    }

    @Test
    public void listOfLinesWillBeReturnedWhenGivenSongString() {
        List<String> lines = songInfoExtractor.filterLines("First line\nSecond line\nThird line");

        assertNotNull(lines, "List of song lines is null");
        assertFalse(lines.isEmpty(), "List of lines is empty");
        assertEquals("First line", lines.get(0), "Song string has been split incorrectly");
        assertEquals("Second line", lines.get(1), "Song string has been split incorrectly");
        assertEquals("Third line", lines.get(2), "Song string has been split incorrectly");
    }

    @Test
    public void listOfTitlesWillBeReturnedWhenGivenWebPageAndTag() {
        doReturn(elementsMock)
                .when(webPageMock)
                .getElementsByTag("h1");
        doReturn(false)
                .when(elementsMock)
                .isEmpty();
        doReturn("First song")
                .when(firstElementMock)
                .text();
        doReturn("Second song")
                .when(secondElementMock)
                .text();
        doAnswer(invocationOnMock -> Stream.of(firstElementMock, secondElementMock))
                .when(elementsMock)
                .stream();

        List<String> titles = songInfoExtractor.extractTitle(webPageMock, "h1");

        assertNotNull(titles, "List of titles is null");
        assertFalse(titles.isEmpty(), "List of titles is empty");
        assertEquals("First song", titles.get(0), "First title is wrong");
        assertEquals("Second song", titles.get(1), "Second title is wrong");

        verify(webPageMock).getElementsByTag("h1");
        verify(elementsMock).isEmpty();
        verify(elementsMock).stream();
        verify(firstElementMock).text();
        verify(secondElementMock).text();
    }

    @Test
    public void listOfSongLinesWillBeReturnedWhenGivenWebPageAndTag() {
        doReturn(elementsMock)
                .when(webPageMock)
                .getElementsByTag("textPesni");
        doReturn(false)
                .when(elementsMock)
                .isEmpty();
        doReturn(firstElementMock)
                .when(elementsMock)
                .first();
        doReturn(new Elements())
                .when(firstElementMock)
                .select("br");
        doReturn(new Elements())
                .when(firstElementMock)
                .select("p");
        doReturn("First line\nSecond line\nThird line")
                .when(firstElementMock)
                .html();

        List<String> lyrics = songInfoExtractor.extractLyrics(webPageMock, "textPesni");

        assertNotNull(lyrics, "List of song lines is null");
        assertFalse(lyrics.isEmpty(), "List of song lines is empty");
        assertEquals("First line", lyrics.get(0), "First line is wrong");
        assertEquals("Second line", lyrics.get(1), "Second line is wrong");
        assertEquals("Third line", lyrics.get(2), "Third line is wrong");

        verify(webPageMock).getElementsByTag("textPesni");
        verify(elementsMock, times(2)).isEmpty();
        verify(elementsMock).first();
        verify(firstElementMock).select("br");
        verify(firstElementMock).select("p");
        verify(firstElementMock).html();
    }

    @Test
    public void listOfSongLinesWillBeReturnedWhenGivenWebPageAndClass() {
        doReturn(new Elements())
                .when(webPageMock)
                .getElementsByTag("songTextTrans songText");
        doReturn(elementsMock)
                .when(webPageMock)
                .getElementsByClass("songTextTrans songText");
        doReturn(false)
                .when(elementsMock)
                .isEmpty();
        doReturn(firstElementMock)
                .when(elementsMock)
                .first();
        doReturn(new Elements())
                .when(firstElementMock)
                .select("br");
        doReturn(new Elements())
                .when(firstElementMock)
                .select("p");
        doReturn("First line\nSecond line\nThird line")
                .when(firstElementMock)
                .html();

        List<String> lyrics = songInfoExtractor.extractLyrics(webPageMock, "songTextTrans songText");

        assertNotNull(lyrics, "List of song lines is null");
        assertFalse(lyrics.isEmpty(), "List of song lines is empty");
        assertEquals("First line", lyrics.get(0), "First line is wrong");
        assertEquals("Second line", lyrics.get(1), "Second line is wrong");
        assertEquals("Third line", lyrics.get(2), "Third line is wrong");

        verify(webPageMock).getElementsByTag("songTextTrans songText");
        verify(webPageMock).getElementsByClass("songTextTrans songText");
        verify(elementsMock).isEmpty();
        verify(elementsMock).first();
        verify(firstElementMock).select("br");
        verify(firstElementMock).select("p");
        verify(firstElementMock).html();
    }
}