package com.likhomanov.services.webpageparsers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AltwallSongInfoExtractorTest {

    @Mock
    private Document webPageMock;
    @Mock
    private Elements contentMock;
    @Mock
    private Element elementMock;
    @InjectMocks
    private AltwallSongInfoExtractor songInfoExtractor;

    @Test
    public void listOfSongLinesWithoutEmptyLinesAndSingleWhiteSpacesWillBeReturned() {
        List<String> lines = songInfoExtractor.filterLines("First line\n\n" +
                                                                 "Second line\n \n" +
                                                                 "Third line\n \n" +
                                                                 "Forth line\n\n" +
                                                                 "Fifth line");

        assertNotNull(lines, "List of song lines is null");
        assertFalse(lines.isEmpty(), "List of song lines is empty");
        assertTrue(lines.get(0).equals("First line") && lines.get(1).equals("Second line"),
                   "Song string has been split wrongly");
        assertFalse(lines.contains(""), "List of song lines contains empty lines");
        assertFalse(lines.contains(" "), "List of song lines contains single white spaces");
    }

    @Test
    public void listWithOneTitleWillBeReturnedWhenGivenWebPageAndTag() {
        doReturn(contentMock)
                .when(webPageMock)
                .getElementsByTag("h1");
        doReturn(false)
                .when(contentMock)
                .isEmpty();
        doReturn(elementMock)
                .when(contentMock)
                .first();
        doReturn("Song title")
                .when(elementMock)
                .text();

        List<String> titles = songInfoExtractor.extractTitle(webPageMock, "h1");

        assertNotNull(titles, "List of titles is null");
        assertFalse(titles.isEmpty(), "List of titles is empty");

        verify(webPageMock).getElementsByTag("h1");
        verify(contentMock).isEmpty();
        verify(contentMock).first();
        verify(elementMock).text();
    }
}