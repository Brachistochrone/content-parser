package com.likhomanov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.UserDao;
import com.likhomanov.dao.entities.UserEntity;
import com.likhomanov.web.dtos.NewUserDto;
import com.likhomanov.web.dtos.UserDto;
import com.likhomanov.web.dtos.UserListDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.PersistenceException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserDao dao;

    @Test
    public void newUserWillBeCreatedAndUserDtoWillBeReturnedWhenGivenNewUserCredentials() throws Exception {
        NewUserDto newUser = new NewUserDto();
        newUser.setEmail("test_mail@gmail.com");
        newUser.setLogin("Seneca");
        newUser.setPassword("qwerty");
        String requestBody = new ObjectMapper().writeValueAsString(newUser);

        doAnswer(invocationOnMock -> {
            UserEntity userEntity = invocationOnMock.getArgument(0);
            userEntity.setId(1L);
            return userEntity;
        }).when(dao).save(any(UserEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().is2xxSuccessful())
                                                  .andReturn()
                                                  .getResponse();
        UserDto savedUser = new ObjectMapper().readValue(response.getContentAsString(), UserDto.class);

        verify(dao).save(any(UserEntity.class));
        assertEquals(1L, savedUser.getId());
        assertEquals("test_mail@gmail.com", savedUser.getEmail());
        assertEquals("Seneca", savedUser.getLogin());
    }

    @Test
    public void userErrorWillBeThrownWhenEmailIsMissing() throws Exception {
        NewUserDto newUser = new NewUserDto();
        newUser.setLogin("Seneca");
        newUser.setPassword("qwerty");
        String requestBody = new ObjectMapper().writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                .contentType(APPLICATION_JSON)
                                                                .content(requestBody))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        assertEquals(400, response.getStatus());
        assertEquals("Email is missing", response.getErrorMessage());
    }

    @Test
    public void userErrorWillBeThrownWhenLoginIsMissing() throws Exception {
        NewUserDto newUser = new NewUserDto();
        newUser.setEmail("test_mail@gmail.com");
        newUser.setPassword("qwerty");
        String requestBody = new ObjectMapper().writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                .contentType(APPLICATION_JSON)
                                                                .content(requestBody))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        assertEquals(400, response.getStatus());
        assertEquals("Login is missing", response.getErrorMessage());
    }

    @Test
    public void userErrorWillBeThrownWhenPasswordIsMissing() throws Exception {
        NewUserDto newUser = new NewUserDto();
        newUser.setEmail("test_mail@gmail.com");
        newUser.setLogin("Seneca");
        String requestBody = new ObjectMapper().writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                .contentType(APPLICATION_JSON)
                                                                .content(requestBody))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        assertEquals(400, response.getStatus());
        assertEquals("Password is missing", response.getErrorMessage());
    }

    @Test
    public void userErrorWillBeThrownWhenProvidedEmailOrLoginAlreadyExist() throws Exception {
        NewUserDto newUser = new NewUserDto();
        newUser.setEmail("test_mail@gmail.com");
        newUser.setLogin("Seneca");
        newUser.setPassword("qwerty");
        String requestBody = new ObjectMapper().writeValueAsString(newUser);

        doThrow(PersistenceException.class).when(dao).save(any(UserEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                .contentType(APPLICATION_JSON)
                                                                .content(requestBody))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        verify(dao).save(any(UserEntity.class));
        assertEquals(400, response.getStatus());
        assertEquals("User with such login or email already exists", response.getErrorMessage());
    }

    @Test
    public void accessWillBeDeniedWhenUnauthorizedUserTriesToGetAllUsers() throws Exception {
        mockMvc.perform(get("/users")).andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void accessWillBeDeniedWhenAuthorisedUserWithUserRoleTriesToGetAllUsers() throws Exception {
        mockMvc.perform(get("/users")).andExpect(status().is4xxClientError());

        verify(dao, never()).findAll();
    }

    @Test
    @WithMockUser(roles = "SUPER_ADMIN")
    public void userListDtoWillBeReturnedWhenAuthorisedUserWithSuperAdminRoleTriesToGetAllUsers() throws Exception {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setEmail("test_mail@gmail.com");
        userEntity.setLogin("Seneca");

        doReturn(Collections.singletonList(userEntity)).when(dao).findAll();

        MockHttpServletResponse response = mockMvc.perform(get("/users"))
                                                  .andExpect(status().is2xxSuccessful())
                                                  .andReturn()
                                                  .getResponse();
        UserListDto userList = new ObjectMapper().readValue(response.getContentAsString(), UserListDto.class);

        verify(dao).findAll();
        assertEquals(1, userList.getUsers().size());
        assertEquals(1, userList.getUsersCount());
        assertEquals(1L, userList.getUsers().get(0).getId());
        assertEquals("test_mail@gmail.com", userList.getUsers().get(0).getEmail());
        assertEquals("Seneca", userList.getUsers().get(0).getLogin());
    }
}