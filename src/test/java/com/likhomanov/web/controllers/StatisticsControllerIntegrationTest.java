package com.likhomanov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.dao.LyricsStatisticsDao;
import com.likhomanov.dao.entities.LyricsStatisticsEntity;
import com.likhomanov.web.dtos.LyricsStatisticsDto;
import com.likhomanov.web.dtos.LyricsStatisticsListDto;
import com.likhomanov.web.dtos.UrlsListDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static com.likhomanov.enums.Language.ENGLISH;
import static com.likhomanov.enums.Language.RUSSIAN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class StatisticsControllerIntegrationTest {

    private final String testUrl = "https://teksty-pesenok.ru/iron-maiden/tekst-pesni-2-minutes-to-midnight/4823360/";
    private final String wrongUrl = "https://www.baeldung.com/mockito-spy";
    private final String illegalUrl = "https://eksty-pesenok.ru/iron-maiden/";
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private LyricsStatisticsDao dao;

    @Test
    public void accessWillBeDeniedWhenUnauthorizedUserSendsRequestWithUrl() throws Exception {
        UrlsListDto urlsDto = new UrlsListDto();
        urlsDto.setUrls(Collections.singletonList(testUrl));
        String requestBody = new ObjectMapper().writeValueAsString(urlsDto);

        mockMvc.perform(put("/statistics")
                        .contentType(APPLICATION_JSON)
                        .content(requestBody))
               .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser
    public void lyricsStatisticsWillBeGatheredAndLyricsStatisticsListDtoWillBeReturnedWhenGivenNewSongUrlByAuthorizedUser() throws Exception {
        UrlsListDto urlsDto = new UrlsListDto();
        urlsDto.setUrls(Collections.singletonList(testUrl));
        String requestBody = new ObjectMapper().writeValueAsString(urlsDto);

        doAnswer(invocationOnMock -> invocationOnMock.<LyricsStatisticsEntity>getArgument(0))
                .when(dao)
                .save(any(LyricsStatisticsEntity.class));

        MockHttpServletResponse response = mockMvc.perform(put("/statistics")
                                                           .contentType(APPLICATION_JSON)
                                                           .content(requestBody))
                                                  .andExpect(status().is2xxSuccessful())
                                                  .andReturn()
                                                  .getResponse();
        LyricsStatisticsListDto statisticsListDto = new ObjectMapper().readValue(response.getContentAsString(),
                                                                                 LyricsStatisticsListDto.class);

        verify(dao, times(2)).save(any(LyricsStatisticsEntity.class));
        assertEquals(1, statisticsListDto.getUrlsCount());
        assertEquals(2, statisticsListDto.getStatisticsDtos().size());
        assertEquals(testUrl, statisticsListDto.getStatisticsDtos().get(0).getUrl());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(0).getContent());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(0).getTotalNumberOfWords());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(0).getNumberOfSameWords());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(0).getNumberOfUniqueWords());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(0).getMostPopularWord());
        assertEquals(ENGLISH, statisticsListDto.getStatisticsDtos().get(0).getLanguage());
        assertEquals(testUrl, statisticsListDto.getStatisticsDtos().get(1).getUrl());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(1).getContent());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(1).getTotalNumberOfWords());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(1).getNumberOfSameWords());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(1).getNumberOfUniqueWords());
        assertNotNull(statisticsListDto.getStatisticsDtos().get(1).getMostPopularWord());
        assertEquals(RUSSIAN, statisticsListDto.getStatisticsDtos().get(1).getLanguage());
    }

    @Test
    @WithMockUser
    public void lyricsStatisticsListDtoWillBeReturnedFromDbWhenGivenAlreadyProcessedSongUrlByAuthorizedUser() throws Exception {
        UrlsListDto urlsDto = new UrlsListDto();
        urlsDto.setUrls(Collections.singletonList(testUrl));
        String requestBody = new ObjectMapper().writeValueAsString(urlsDto);
        LyricsStatisticsEntity statistics = new LyricsStatisticsEntity();
        statistics.setUrl(testUrl);

        doReturn(Optional.of(Collections.singletonList(statistics)))
                .when(dao)
                .findAllByUrl(testUrl);

        MockHttpServletResponse response = mockMvc.perform(put("/statistics")
                                                               .contentType(APPLICATION_JSON)
                                                               .content(requestBody))
                                                  .andExpect(status().is2xxSuccessful())
                                                  .andReturn()
                                                  .getResponse();
        LyricsStatisticsListDto statisticsListDto = new ObjectMapper().readValue(response.getContentAsString(),
                                                                                 LyricsStatisticsListDto.class);

        verify(dao).findAllByUrl(testUrl);
        verify(dao, never()).save(any(LyricsStatisticsEntity.class));
        assertEquals(1, statisticsListDto.getUrlsCount());
        assertEquals(1, statisticsListDto.getStatisticsDtos().size());
        assertEquals(testUrl, statisticsListDto.getStatisticsDtos().get(0).getUrl());
    }

    @Test
    @WithMockUser
    public void onlyOneSongUrlWillBeProcessedWhenGivenDuplicatedUrlsByAuthorizedUser() throws Exception {
        UrlsListDto urlsDto = new UrlsListDto();
        urlsDto.setUrls(Arrays.asList(testUrl, testUrl));
        String requestBody = new ObjectMapper().writeValueAsString(urlsDto);

        doAnswer(invocationOnMock -> invocationOnMock.<LyricsStatisticsEntity>getArgument(0))
                .when(dao)
                .save(any(LyricsStatisticsEntity.class));

        MockHttpServletResponse response = mockMvc.perform(put("/statistics")
                                                               .contentType(APPLICATION_JSON)
                                                               .content(requestBody))
                                                  .andExpect(status().is2xxSuccessful())
                                                  .andReturn()
                                                  .getResponse();
        LyricsStatisticsListDto statisticsListDto = new ObjectMapper().readValue(response.getContentAsString(),
                                                                                 LyricsStatisticsListDto.class);

        verify(dao, times(2)).save(any(LyricsStatisticsEntity.class));
        assertEquals(2, statisticsListDto.getStatisticsDtos().size());
    }

    @Test
    @WithMockUser
    public void userErrorMessageWillBeReturnedWhenGivenUrlOfUnsupportedWebSiteByAuthorisedUser() throws Exception {
        UrlsListDto urlsDto = new UrlsListDto();
        urlsDto.setUrls(Collections.singletonList(wrongUrl));
        String requestBody = new ObjectMapper().writeValueAsString(urlsDto);

        doAnswer(invocationOnMock -> invocationOnMock.<LyricsStatisticsEntity>getArgument(0))
                .when(dao)
                .save(any(LyricsStatisticsEntity.class));

        MockHttpServletResponse response = mockMvc.perform(put("/statistics")
                                                               .contentType(APPLICATION_JSON)
                                                               .content(requestBody))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        verify(dao).save(any(LyricsStatisticsEntity.class));
        assertEquals(400, response.getStatus());
        assertEquals("Unsupported web site", response.getErrorMessage());
    }

    @Test
    @WithMockUser
    public void userErrorMessageWillBeReturnedWhenGivenIllegalUrlByAuthorisedUser() throws Exception {
        UrlsListDto urlsDto = new UrlsListDto();
        urlsDto.setUrls(Collections.singletonList(illegalUrl));
        String requestBody = new ObjectMapper().writeValueAsString(urlsDto);

        doAnswer(invocationOnMock -> invocationOnMock.<LyricsStatisticsEntity>getArgument(0))
                .when(dao)
                .save(any(LyricsStatisticsEntity.class));

        MockHttpServletResponse response = mockMvc.perform(put("/statistics")
                                                               .contentType(APPLICATION_JSON)
                                                               .content(requestBody))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        verify(dao).save(any(LyricsStatisticsEntity.class));
        assertEquals(400, response.getStatus());
        assertEquals("Cannot fetch Web page", response.getErrorMessage());
    }

    @Test
    public void accessWillBeDeniedWhenUnauthorizedUserIsTryingToGetStatisticsById() throws Exception {
        mockMvc.perform(get("/statistics/1"))
               .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser
    public void userErrorMessageWillBeReturnedWhenGivenWrongIdByAuthorizedUser() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/statistics/1"))
                                                  .andExpect(status().is4xxClientError())
                                                  .andReturn()
                                                  .getResponse();

        verify(dao).findById(1L);
        assertEquals(404, response.getStatus());
        assertEquals("Statistics not found", response.getErrorMessage());
    }

    @Test
    @WithMockUser
    public void lyricsStatisticsDtoWillBeReturnedWhenGivenCorrectIdByAuthorizedUser() throws Exception {
        doAnswer(invocationOnMock -> {
            LyricsStatisticsEntity statistics = new LyricsStatisticsEntity();
            statistics.setId(invocationOnMock.getArgument(0));
            return Optional.of(statistics);
        }).when(dao).findById(anyLong());

        MockHttpServletResponse response = mockMvc.perform(get("/statistics/1"))
                                                  .andExpect(status().is2xxSuccessful())
                                                  .andReturn()
                                                  .getResponse();
        LyricsStatisticsDto statisticsDto = new ObjectMapper().readValue(response.getContentAsString(),
                                                                         LyricsStatisticsDto.class);

        verify(dao).findById(anyLong());
        assertEquals(1L, statisticsDto.getId());
    }
}