package com.likhomanov.exceptions;

public class WordFunctionException extends ContentParserException {

    public WordFunctionException(String message) {
        super(message);
    }

    @Override
    public String getExceptionType() {
        return "WordFunctionException";
    }
}
