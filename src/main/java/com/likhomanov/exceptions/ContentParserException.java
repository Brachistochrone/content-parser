package com.likhomanov.exceptions;

public abstract class ContentParserException extends RuntimeException {

    public ContentParserException(String message) {
        super(message);
    }

    public ContentParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public abstract String getExceptionType();
}
