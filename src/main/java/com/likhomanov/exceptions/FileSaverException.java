package com.likhomanov.exceptions;

public class FileSaverException extends ContentParserException {

    public FileSaverException(String message) {
        super(message);
    }

    public FileSaverException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getExceptionType() {
        return "FileSaverException";
    }
}
