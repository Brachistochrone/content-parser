package com.likhomanov.exceptions;

public class UserNotFoundException extends ContentParserException {

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getExceptionType() {
        return "UserNotFoundException";
    }
}
