package com.likhomanov.exceptions;

public class DaoException extends ContentParserException {

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getExceptionType() {
        return "DaoException";
    }
}
