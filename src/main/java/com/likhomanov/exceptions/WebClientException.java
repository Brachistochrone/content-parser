package com.likhomanov.exceptions;

public class WebClientException extends ContentParserException {

    public WebClientException(String message) {
        super(message);
    }

    public WebClientException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getExceptionType() {
        return "WebClientException";
    }
}
