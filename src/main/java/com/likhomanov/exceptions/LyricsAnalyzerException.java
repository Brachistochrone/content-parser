package com.likhomanov.exceptions;

public class LyricsAnalyzerException extends ContentParserException {

    public LyricsAnalyzerException(String message) {
        super(message);
    }

    @Override
    public String getExceptionType() {
        return "LyricsAnalyzerException";
    }
}
