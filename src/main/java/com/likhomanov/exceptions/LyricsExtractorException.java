package com.likhomanov.exceptions;

public class LyricsExtractorException extends ContentParserException {

    public LyricsExtractorException(String message) {
        super(message);
    }

    public LyricsExtractorException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getExceptionType() {
        return "LyricsExtractorException";
    }
}
