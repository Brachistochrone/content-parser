package com.likhomanov.services;

import com.likhomanov.dao.UserDao;
import com.likhomanov.dao.entities.UserEntity;
import com.likhomanov.enums.Role;
import com.likhomanov.exceptions.UserNotFoundException;
import com.likhomanov.model.User;
import com.likhomanov.model.UserInstance;
import com.likhomanov.model.UserRoleInstance;
import com.likhomanov.services.mappers.InstanceEntityDtoMapper;
import com.likhomanov.web.dtos.NewUserDto;
import com.likhomanov.web.dtos.UserDto;
import com.likhomanov.web.dtos.UserListDto;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.PersistenceException;
import java.util.Optional;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class UserService implements UserDetailsService {

    private final UserDao userDao;
    private final InstanceEntityDtoMapper mapper;

    public UserService(UserDao userDao, InstanceEntityDtoMapper mapper) {
        this.userDao = userDao;
        this.mapper = mapper;
    }

    public UserDto saveNewUser(NewUserDto newUser) {
        try {
            checkUserCredentials(newUser);
            UserInstance newUserWithRole = appointRole(mapper.newUserDtoToInstance(newUser));
            User savedUser = userDao.save(mapper.userToEntity(newUserWithRole));
            return mapper.userToDto(savedUser);
        } catch (PersistenceException e) {
            throw new ResponseStatusException(BAD_REQUEST, "User with such login or email already exists");
        }
    }

    @Secured("ROLE_SUPER_ADMIN")
    public UserListDto getAllUsers() {
        Iterable<UserEntity> allUsers = userDao.findAll();
        return mapper.usersToUserListDto(allUsers);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<? extends User> optionalUser = userDao.findOneByEmail(email);
        User foundUser = optionalUser.orElseThrow(() -> new UserNotFoundException("User with email: "
                                                                                  + email
                                                                                  + " not found"));
        return new org.springframework.security.core.userdetails.User(foundUser.getEmail(),
                                                                      foundUser.getPassword(),
                                                                      mapper.userRolesToAuthorities(foundUser.getRoles()));
    }

    private UserInstance appointRole(UserInstance newUser) {
        UserRoleInstance role = new UserRoleInstance();
        role.setRole(Role.ROLE_USER);
        newUser.addRole(role);
        return newUser;
    }

    private void checkUserCredentials(NewUserDto user) {
        if (user.getEmail() == null) throw new ResponseStatusException(BAD_REQUEST, "Email is missing");
        if (user.getLogin() == null) throw new ResponseStatusException(BAD_REQUEST, "Login is missing");
        if (user.getPassword() == null) throw new ResponseStatusException(BAD_REQUEST, "Password is missing");
    }
}
