package com.likhomanov.services;

import com.likhomanov.exceptions.LyricsExtractorException;
import com.likhomanov.model.Lyrics;
import com.likhomanov.services.webpageparsers.WebPageParser;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LyricsExtractor {

    private final List<WebPageParser> parsers;

    public LyricsExtractor(List<WebPageParser> parsers) {
        this.parsers = parsers;
    }

    public List<Lyrics> extractLyrics(Document webPage) {
        if (webPage == null) throw new LyricsExtractorException("Web page is null");

        for (WebPageParser parser : parsers) {
            if (!webPage.location().contains(parser.getTargetUrl())) continue;
            return parser.parseWebPage(webPage);
        }
        throw new LyricsExtractorException("Unsupported web site");
    }
}
