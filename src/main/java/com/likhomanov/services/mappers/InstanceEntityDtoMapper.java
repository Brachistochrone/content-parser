package com.likhomanov.services.mappers;

import com.likhomanov.dao.entities.LyricsExceptionEntity;
import com.likhomanov.dao.entities.LyricsStatisticsEntity;
import com.likhomanov.dao.entities.UserEntity;
import com.likhomanov.dao.entities.UserRoleEntity;
import com.likhomanov.enums.Role;
import com.likhomanov.model.*;
import com.likhomanov.web.dtos.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Mapper(componentModel = "spring")
public abstract class InstanceEntityDtoMapper {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Mapping(source = "lyrics", target = "content")
    public abstract LyricsStatisticsDto lyricsStatisticsToDto(LyricsStatistics statistics);

    public abstract LyricsStatisticsEntity lyricsStatisticsToEntity(LyricsStatistics statistics);

    public abstract LyricsExceptionEntity lyricsExceptionToEntity(LyricsException exception);

    public abstract UserDto userToDto(User user);

    public UserInstance newUserDtoToInstance(NewUserDto newUserDto) {
        if ( newUserDto == null ) {
            return null;
        }

        UserInstance userInstance = new UserInstance();

        userInstance.setId( newUserDto.getId() );
        userInstance.setEmail( newUserDto.getEmail() );
        String encodedPassword = passwordEncoder.encode(newUserDto.getPassword());
        userInstance.setPassword( encodedPassword );
        userInstance.setLogin( newUserDto.getLogin() );

        return userInstance;
    }

    public UserEntity userToEntity(User user) {
        if ( user == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setId( user.getId() );
        userEntity.setEmail( user.getEmail() );
        userEntity.setPassword( user.getPassword() );
        userEntity.setLogin( user.getLogin() );
        userEntity.setRoles( userRoleSetToUserRoleEntitySet( user.getRoles(), userEntity ) );

        return userEntity;
    }

    public UserRoleEntity userRoleToEntity(UserRole role, UserEntity user) {
        if ( role == null ) {
            return null;
        }

        UserRoleEntity userRoleEntity = new UserRoleEntity();

        userRoleEntity.setId( role.getId() );
        userRoleEntity.setRole( role.getRole() );
        userRoleEntity.setUser( user );

        return userRoleEntity;
    }

    protected Set<UserRoleEntity> userRoleSetToUserRoleEntitySet(Set<? extends UserRole> set, UserEntity user) {
        if ( set == null ) {
            return null;
        }

        Set<UserRoleEntity> set1 = new HashSet<UserRoleEntity>(Math.max((int) ( set.size() / .75f ) + 1, 16) );
        for ( UserRole userRole : set ) {
            set1.add( userRoleToEntity( userRole, user ) );
        }

        return set1;
    }

    public Collection<? extends GrantedAuthority> userRolesToAuthorities(Set<? extends UserRole> roles) {
        return roles.stream()
                    .map(this::userRoleToAuthority)
                    .collect(Collectors.toSet());
    }

    protected GrantedAuthority userRoleToAuthority(UserRole role) {
        return roleToAuthority(role.getRole());
    }

    public GrantedAuthority roleToAuthority(Role role) {
        return new SimpleGrantedAuthority(role.name());
    }

    @Mapping(source = "username", target = "email")
    @Mapping(source = "authorities", target = "roles")
    public abstract AuthenticatedUserDto springUserToAuthenticatedUser(org.springframework.security.core.userdetails.User user);

    protected Role authorityToRole(GrantedAuthority authority) {
        return Role.valueOf(authority.getAuthority());
    }

    public UserListDto usersToUserListDto(Iterable<? extends User> users) {
        UserListDto userListDto = new UserListDto();

        List<UserDto> userDtos = StreamSupport.stream(users.spliterator(), false)
                                              .map(this::userToDto)
                                              .collect(Collectors.toList());
        userListDto.setUsersCount(userDtos.size());
        userListDto.setUsers(userDtos);
        return userListDto;
    }
}
