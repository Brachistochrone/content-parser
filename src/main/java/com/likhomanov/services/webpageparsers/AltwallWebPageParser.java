package com.likhomanov.services.webpageparsers;

import com.likhomanov.model.Lyrics;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Component
public class AltwallWebPageParser implements WebPageParser {

    private final SongInfoExtractor infoExtractor;

    public AltwallWebPageParser(@Qualifier("altwallSongInfoExtractor")
                                SongInfoExtractor infoExtractor) {
        this.infoExtractor = infoExtractor;
    }

    @Override
    public List<Lyrics> parseWebPage(@NotNull Document webPage) {
        List<String> lyrics = infoExtractor.extractLyrics(webPage, "article");
        List<String> titles = infoExtractor.extractTitle(webPage, "h1");
        return Collections.singletonList(getLyrics(webPage.location(), titles.get(0), lyrics));
    }

    private Lyrics getLyrics(String url, String title, List<String> lines) {
        return new Lyrics(url, title, lines);
    }

    @Override
    public String getTargetUrl() {
        return "altwall.net";
    }
}
