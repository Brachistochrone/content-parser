package com.likhomanov.services.webpageparsers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TekstyPesenokTranslatedSongInfoExtractor extends WebPageSongInfoExtractor {

    @Override
    public List<String> extractLyrics(Document webPage, String tag) {
        Elements content = webPage.getElementsByClass(tag);

        validateContent(content, tag);

        List<String> lyrics = content.stream()
                                     .map(Element::text)
                                     .collect(Collectors.toList());
        lyrics.remove("Источник teksty-pesenok.ru");

        return lyrics;
    }
}
