package com.likhomanov.services.webpageparsers;

import com.likhomanov.exceptions.LyricsExtractorException;
import com.likhomanov.model.Lyrics;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class TekstyPesenokWebPageParser implements WebPageParser {

    private final SongInfoExtractor originalInfoExtractor;
    private final SongInfoExtractor translatedInfoExtractor;

    public TekstyPesenokWebPageParser(@Qualifier("tekstyPesenokOriginalSongInfoExtractor")
                                      SongInfoExtractor originalInfoExtractor,
                                      @Qualifier("tekstyPesenokTranslatedSongInfoExtractor")
                                      SongInfoExtractor translatedInfoExtractor) {
        this.originalInfoExtractor = originalInfoExtractor;
        this.translatedInfoExtractor = translatedInfoExtractor;
    }

    @Override
    public List<Lyrics> parseWebPage(@NotNull Document webPage) {
        if (webPage.selectFirst("span.songTextTrans.songText") != null) {
            List<String> originalText = translatedInfoExtractor.extractLyrics(webPage,
                                                                              "songTextTrans songText");
            List<String> translatedText = translatedInfoExtractor.extractLyrics(webPage,
                                                                                "songTextTrans songTrans");
            List<String> titles = translatedInfoExtractor.extractTitle(webPage,"h1");

            Lyrics originalLyrics = new Lyrics(webPage.location(), titles.get(0), originalText);
            Lyrics translatedLyrics = new Lyrics(webPage.location(), titles.get(1), translatedText);

            return Arrays.asList(originalLyrics, translatedLyrics);
        } else if (webPage.selectFirst("div.textPesni") != null) {
            List<String> originalText = originalInfoExtractor.extractLyrics(webPage,"textPesni");
            List<String> titles = originalInfoExtractor.extractTitle(webPage,"h1");

            Lyrics originalLyrics = new Lyrics(webPage.location(), titles.get(0), originalText);

            return Collections.singletonList(originalLyrics);
        } else {
            throw new LyricsExtractorException("Unsupported web page format");
        }
    }

    @Override
    public String getTargetUrl() {
        return "teksty-pesenok.ru";
    }
}
