package com.likhomanov.services.webpageparsers;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TekstyPesenokOriginalSongInfoExtractor extends WebPageSongInfoExtractor {

    @Override
    public List<String> filterLines(String lyrics) {
        String[] lines = lyrics.split("\n");
        return Arrays.stream(lines)
                     .filter(line -> !line.equals("") &&
                             !line.equals(" ") &&
                             !line.equals("  ") &&
                             !line.contains("teksty-pesenok.ru"))
                     .collect(Collectors.toList());
    }
}
