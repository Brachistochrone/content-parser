package com.likhomanov.services.webpageparsers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AltwallSongInfoExtractor extends WebPageSongInfoExtractor {

    @Override
    public List<String> extractTitle(@NotNull Document webPage, String tag) {
        Elements content = webPage.getElementsByTag(tag);

        validateContent(content, tag);

        Element element = content.first();

        return Collections.singletonList(element.text());
    }

    @Override
    public List<String> filterLines(String lyrics) {
        String[] lines = lyrics.split("\n");
        return Arrays.stream(lines)
                     .filter(line -> !line.equals("") &&
                             !line.equals(" "))
                     .collect(Collectors.toList());
    }
}
