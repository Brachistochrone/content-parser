package com.likhomanov.services.webpageparsers;

import com.likhomanov.exceptions.LyricsExtractorException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class WebPageSongInfoExtractor implements SongInfoExtractor {

    @Override
    public List<String> extractTitle(Document webPage, String tag) {
        Elements content = webPage.getElementsByTag(tag);

        validateContent(content, tag);

        return content.stream()
                      .map(Element::text)
                      .collect(Collectors.toList());
    }

    @Override
    public List<String> extractLyrics(Document webPage, String tag) {
        Elements content = webPage.getElementsByTag(tag);

        if (content.isEmpty()) {
            content = webPage.getElementsByClass(tag);
        }

        validateContent(content, tag);

        Element element = content.first();
        element.select("br").append("\\n");
        element.select("p").append("\\n\\n");
        String html = element.html().replaceAll("\\\\n", "\n");

        String cleanLyrics = Jsoup.clean(html,
                                         "",
                                         Whitelist.none(),
                                         new Document.OutputSettings().prettyPrint(false));
        return filterLines(cleanLyrics);
    }

    List<String> filterLines(String lyrics) {
        String[] lines = lyrics.split("\n");
        return Arrays.stream(lines)
                     .collect(Collectors.toList());
    }

    void validateContent(Elements content, String tag) {
        if (content == null || content.isEmpty()) throw new LyricsExtractorException("Tag " + tag + " not found");
    }
}
