package com.likhomanov.services.webpageparsers;

import com.likhomanov.model.Lyrics;
import org.jsoup.nodes.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface WebPageParser {

    List<Lyrics> parseWebPage(@NotNull Document webPage);

    String getTargetUrl();
}
