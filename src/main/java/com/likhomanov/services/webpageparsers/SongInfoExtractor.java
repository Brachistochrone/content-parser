package com.likhomanov.services.webpageparsers;

import org.jsoup.nodes.Document;

import java.util.List;

public interface SongInfoExtractor {

    List<String> extractTitle(Document webPage, String tag);

    List<String> extractLyrics(Document webPage, String tag);
}
