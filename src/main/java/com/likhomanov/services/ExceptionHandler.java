package com.likhomanov.services;

import com.likhomanov.dao.LyricsStatisticsDao;
import com.likhomanov.model.LyricsExceptionInstance;
import com.likhomanov.model.LyricsStatistics;
import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.services.mappers.InstanceEntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class ExceptionHandler {

    private final LyricsStatisticsDao dao;
    private final InstanceEntityDtoMapper mapper;

    public ExceptionHandler(LyricsStatisticsDao dao, InstanceEntityDtoMapper mapper) {
        this.dao = dao;
        this.mapper = mapper;
    }

    public LyricsStatistics handleException(Throwable e, String url) {
        LyricsExceptionInstance exceptionInstance = new LyricsExceptionInstance();
        exceptionInstance.setExceptionType(e.getCause().getClass().toString());
        exceptionInstance.setExceptionMessage(e.getCause().getMessage());

        LyricsStatisticsInstance statistics = new LyricsStatisticsInstance();
        statistics.setDate(Calendar.getInstance().getTime());
        statistics.setTimestamp(System.currentTimeMillis());
        statistics.setUrl("exp_" + url);
        statistics.setLyricsException(exceptionInstance);

        return dao.save(mapper.lyricsStatisticsToEntity(statistics));
    }
}
