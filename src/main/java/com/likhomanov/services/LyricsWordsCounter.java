package com.likhomanov.services;

import com.likhomanov.model.Lyrics;
import com.likhomanov.model.Word;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Component
public class LyricsWordsCounter {

    public Map<String, Word> countWords(@NotNull Lyrics lyrics) {
        Map<String, Word> words = new HashMap<>();

        for (String line : lyrics.getLines()) {
            line = line.toLowerCase();
            for (String word : line.split("[!?:\\-.,\\s]+")) {
                if (word.equals("")) continue;
                if (words.get(word) == null) words.put(word, new Word(word));
                else words.get(word).count();
            }
        }
        return words;
    }
}
