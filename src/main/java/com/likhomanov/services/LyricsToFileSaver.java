package com.likhomanov.services;

import com.likhomanov.exceptions.FileSaverException;
import com.likhomanov.model.Lyrics;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Component
public class LyricsToFileSaver {

    private final String pathToDirectory;

    public LyricsToFileSaver(@Value("${local.file.path}") String pathToDirectory) {
        this.pathToDirectory = pathToDirectory;
    }

    public void storeToFile(List<Lyrics> lyricsList) {
        for (Lyrics lyrics : lyricsList) {
            File file = new File(pathToDirectory + lyrics.getTitle() + ".txt");

            try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(lyrics.getUrl());
                writer.newLine();

                for (String line : lyrics.getLines()) {
                    writer.write(line);
                    writer.newLine();
                }
            } catch (IOException e) {
                throw new FileSaverException("Cannot save to file", e);
            }
        }
    }
}
