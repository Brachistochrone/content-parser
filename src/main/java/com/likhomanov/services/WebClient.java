package com.likhomanov.services;

import com.likhomanov.exceptions.WebClientException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class WebClient {

    public Document fetchWebPage(String url) {
        try {

            return Jsoup.connect(url).get();

        } catch (IOException e) {
            throw new WebClientException("Cannot fetch Web page", e);
        }
    }
}
