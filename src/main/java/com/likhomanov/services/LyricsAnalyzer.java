package com.likhomanov.services;

import com.likhomanov.exceptions.LyricsAnalyzerException;
import com.likhomanov.model.Lyrics;
import com.likhomanov.model.LyricsStatistics;
import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import com.likhomanov.services.wordfunctions.WordFunction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Component
public class LyricsAnalyzer {

    private final List<WordFunction> wordFunctions;
    private final LyricsWordsCounter wordsCounter;

    public LyricsAnalyzer(List<WordFunction> wordFunctions, LyricsWordsCounter wordsCounter) {
        this.wordFunctions = wordFunctions;
        this.wordsCounter = wordsCounter;
    }

    public List<LyricsStatistics> analyze(List<Lyrics> lyricsList) {

        validate(lyricsList);

        List<LyricsStatistics> statisticsList = new ArrayList<>();

        for (Lyrics lyrics : lyricsList) {
            LyricsStatisticsInstance statistics = new LyricsStatisticsInstance();
            Map<String, Word> words = wordsCounter.countWords(lyrics);

            statistics.setDate(Calendar.getInstance().getTime());
            statistics.setTimestamp(System.currentTimeMillis());
            statistics.setUrl(lyrics.getUrl());
            statistics.setLyrics(String.join("\n", lyrics.getLines()));

            for (WordFunction wordFunction : wordFunctions) {
                wordFunction.apply(words, statistics);
            }

            statisticsList.add(statistics);
        }

        return statisticsList;
    }

    private void validate(List<Lyrics> lyricsList) {
        if (lyricsList == null) throw new LyricsAnalyzerException("List of lyrics is null");
        if (lyricsList.isEmpty()) throw new LyricsAnalyzerException("List of lyrics is empty");
    }
}
