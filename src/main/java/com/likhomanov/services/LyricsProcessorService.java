package com.likhomanov.services;

import com.likhomanov.exceptions.ContentParserException;
import com.likhomanov.model.LyricsStatistics;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class LyricsProcessorService {

    private final AsyncLyricsProcessorService asyncLyricsProcessorService;
    private final ExceptionHandler exceptionHandler;

    public LyricsProcessorService(AsyncLyricsProcessorService asyncLyricsProcessorService,
                                  ExceptionHandler exceptionHandler) {
        this.asyncLyricsProcessorService = asyncLyricsProcessorService;
        this.exceptionHandler = exceptionHandler;
    }

    public List<LyricsStatistics> process(List<String> urls) {
        List<LyricsStatistics> statistics = new ArrayList<>();
        Map<String, Future<List<LyricsStatistics>>> futures = urls.stream()
                                                                  .distinct()
                                                                  .collect(Collectors.toMap(url -> url,
                                                                          asyncLyricsProcessorService::process));
        futures.forEach((key, value) -> {
            try {
                statistics.addAll(value.get());
            } catch (ContentParserException | InterruptedException | ExecutionException e) {
                statistics.add(exceptionHandler.handleException(e, key));
            }
        });
        return statistics;
    }
}
