package com.likhomanov.services;

import com.likhomanov.dao.LyricsStatisticsDao;
import com.likhomanov.model.Lyrics;
import com.likhomanov.model.LyricsStatistics;
import com.likhomanov.services.mappers.InstanceEntityDtoMapper;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class AsyncLyricsProcessorService {

    private final WebClient webClient;
    private final LyricsExtractor lyricsExtractor;
    private final LyricsToFileSaver fileSaver;
    private final LyricsAnalyzer analyzer;
    private final LyricsStatisticsDao dao;
    private final ThreadPoolTaskExecutor executor;
    private final InstanceEntityDtoMapper mapper;

    public AsyncLyricsProcessorService(WebClient webClient,
                                       LyricsExtractor lyricsExtractor,
                                       LyricsToFileSaver fileSaver,
                                       LyricsAnalyzer analyzer,
                                       LyricsStatisticsDao dao,
                                       @Qualifier("taskExecutor") ThreadPoolTaskExecutor executor,
                                       InstanceEntityDtoMapper mapper) {
        this.webClient = webClient;
        this.lyricsExtractor = lyricsExtractor;
        this.fileSaver = fileSaver;
        this.analyzer = analyzer;
        this.dao = dao;
        this.executor = executor;
        this.mapper = mapper;
    }

    public Future<List<LyricsStatistics>> process(String url) {
        return executor.submit(() -> {
            List<LyricsStatistics> processedStat = assureProcessed(url);

            if (processedStat.isEmpty()) {
                Document webPage = webClient.fetchWebPage(url);
                List<Lyrics> lyricsList = lyricsExtractor.extractLyrics(webPage);
                fileSaver.storeToFile(lyricsList);
                List<LyricsStatistics> newStatistics = analyzer.analyze(lyricsList);
                return save(newStatistics);
            } else {
                return processedStat;
            }
        });
    }

    private List<LyricsStatistics> assureProcessed(String url) {
        return dao.findAllByUrl(url).orElseGet(Collections::emptyList);
    }

    private List<LyricsStatistics> save(List<LyricsStatistics> statistics) {
        return statistics.stream()
                         .map(stat -> dao.save(mapper.lyricsStatisticsToEntity(stat)))
                         .collect(Collectors.toList());
    }
}
