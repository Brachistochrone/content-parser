package com.likhomanov.services;

import com.likhomanov.model.LyricsException;
import com.likhomanov.model.LyricsStatistics;
import com.likhomanov.dao.LyricsStatisticsDao;
import com.likhomanov.services.mappers.InstanceEntityDtoMapper;
import com.likhomanov.web.dtos.LyricsStatisticsDto;
import com.likhomanov.web.dtos.LyricsStatisticsListDto;
import com.likhomanov.web.dtos.UrlsListDto;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@Service
public class StatisticsService {

    private final LyricsProcessorService lyricsProcessor;
    private final InstanceEntityDtoMapper mapper;
    private final LyricsStatisticsDao dao;

    public StatisticsService(LyricsProcessorService lyricsProcessor,
                             InstanceEntityDtoMapper mapper, LyricsStatisticsDao dao) {
        this.lyricsProcessor = lyricsProcessor;
        this.mapper = mapper;
        this.dao = dao;
    }

    public LyricsStatisticsListDto analyzeLyrics(UrlsListDto urls) {
        List<LyricsStatistics> statistics = lyricsProcessor.process(urls.getUrls());

        final LyricsStatisticsListDto statisticsListDto = new LyricsStatisticsListDto();

        statistics.forEach(stat -> {
            if (stat.getLyricsException() != null) {
                throw createStatusError(stat.getLyricsException());
            }
            statisticsListDto.addStatisticsDto(mapper.lyricsStatisticsToDto(stat));
        });

        statisticsListDto.setUrlsCount(urls.getUrls().size());

        return statisticsListDto;
    }

    public LyricsStatisticsDto findStatisticsById(Long id) {
        LyricsStatistics foundStatistics = dao.findById(id)
                                              .orElseThrow(() -> new ResponseStatusException(NOT_FOUND,
                                                                                             "Statistics not found"));
        return mapper.lyricsStatisticsToDto(foundStatistics);
    }

    private ResponseStatusException createStatusError(LyricsException exception) {
        if (exception.getExceptionType().contains("DaoException")) {
            return new ResponseStatusException(INTERNAL_SERVER_ERROR, exception.getExceptionMessage());
        } else if (exception.getExceptionType().contains("FileSaverException")) {
            return new ResponseStatusException(INTERNAL_SERVER_ERROR, exception.getExceptionMessage());
        } else if (exception.getExceptionType().contains("LyricsExtractorException")) {
            return new ResponseStatusException(BAD_REQUEST, exception.getExceptionMessage());
        } else if (exception.getExceptionType().contains("WebClientException")) {
            return new ResponseStatusException(BAD_REQUEST, exception.getExceptionMessage());
        } else {
            return new ResponseStatusException(INTERNAL_SERVER_ERROR, exception.getExceptionMessage());
        }
    }
}
