package com.likhomanov.services.wordfunctions;

import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MostPopularWordFinder implements WordFunction {

    @Override
    public LyricsStatisticsInstance apply(Map<String, Word> words, LyricsStatisticsInstance statistics) {

        validateParameters(words, statistics);

        Word word = words.values()
                         .stream()
                         .max(Word::compareTo)
                         .get();
        statistics.setMostPopularWord(word.getWord());
        return statistics;
    }
}
