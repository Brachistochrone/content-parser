package com.likhomanov.services.wordfunctions;

import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UniqueWordsCounter implements WordFunction {

    @Override
    public LyricsStatisticsInstance apply(Map<String, Word> words, LyricsStatisticsInstance statistics) {

        validateParameters(words, statistics);

        Long number = words.values()
                           .stream()
                           .filter(word -> word.getQuantity() == 1)
                           .count();
        statistics.setNumberOfUniqueWords(number);
        return statistics;
    }
}
