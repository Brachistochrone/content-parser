package com.likhomanov.services.wordfunctions;

import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AllWordsCounter implements WordFunction {

    @Override
    public LyricsStatisticsInstance apply(Map<String, Word> words, LyricsStatisticsInstance statistics) {

        validateParameters(words, statistics);

        Long number = (long)  words.values()
                                   .stream()
                                   .mapToInt(Word::getQuantity)
                                   .sum();
        statistics.setTotalNumberOfWords(number);
        return statistics;
    }
}
