package com.likhomanov.services.wordfunctions;

import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.likhomanov.enums.Language.*;

@Component
public class LanguageDistinguisher implements WordFunction {

    private final List<String> englishKeyWords = Arrays.asList("the", "of", "and", "a", "to");
    private final List<String> russianKeyWords = Arrays.asList("и", "в", "не", "на", "я");

    private final KeyWordCounter keyWordCounter;

    public LanguageDistinguisher(KeyWordCounter keyWordCounter) {
        this.keyWordCounter = keyWordCounter;
    }

    @Override
    public LyricsStatisticsInstance apply(Map<String, Word> words, LyricsStatisticsInstance statistics) {

        validateParameters(words, statistics);

        if (keyWordCounter.count(words, englishKeyWords) > 10) statistics.setLanguage(ENGLISH);
        else if (keyWordCounter.count(words, russianKeyWords) > 10) statistics.setLanguage(RUSSIAN);
        else statistics.setLanguage(OTHER);
        return statistics;
    }

    public List<String> getEnglishKeyWords() {
        return englishKeyWords;
    }

    public List<String> getRussianKeyWords() {
        return russianKeyWords;
    }
}
