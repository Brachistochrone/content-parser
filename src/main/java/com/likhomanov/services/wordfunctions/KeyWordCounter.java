package com.likhomanov.services.wordfunctions;

import com.likhomanov.exceptions.WordFunctionException;
import com.likhomanov.model.Word;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Component
public class KeyWordCounter {

    public int count(@NotNull Map<String, Word> words, List<String> keyWords) {
        validateList(keyWords);
        int numberOfMatches = 0;
        for (String word : words.keySet()) {
            for (String keyWord : keyWords) {
                if (word.equalsIgnoreCase(keyWord)) numberOfMatches += words.get(word).getQuantity();
            }
        }
        return numberOfMatches;
    }

    private void validateList(List<String> strings) {
        if (strings == null) throw new WordFunctionException("List of key words is null");
        if (strings.isEmpty()) throw new WordFunctionException("List of key words should contain at least one word");
        if (strings.contains("")) throw new WordFunctionException("List of key words contains empty strings");
        if (strings.contains(" ")) throw new WordFunctionException("List of key words contains white spaces");
    }
}
