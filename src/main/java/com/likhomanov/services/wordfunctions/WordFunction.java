package com.likhomanov.services.wordfunctions;

import com.likhomanov.exceptions.WordFunctionException;
import com.likhomanov.model.LyricsStatisticsInstance;
import com.likhomanov.model.Word;

import java.util.Map;

public interface WordFunction {

    LyricsStatisticsInstance apply(Map<String, Word> words, LyricsStatisticsInstance statistics);

    default void validateParameters(Map<String, Word> words, LyricsStatisticsInstance statistics) {
        if (words == null) throw new WordFunctionException("Map of words is null");
        if (statistics == null) throw new WordFunctionException("Statistics is null");
        if (words.isEmpty()) throw new WordFunctionException("Map of words is empty");
    }
}
