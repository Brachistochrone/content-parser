package com.likhomanov.dao;

import com.likhomanov.dao.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findOneByEmail(String email);
}
