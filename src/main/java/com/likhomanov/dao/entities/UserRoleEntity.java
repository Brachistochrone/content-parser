package com.likhomanov.dao.entities;

import com.likhomanov.enums.Role;
import com.likhomanov.model.User;
import com.likhomanov.model.UserRole;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_roles")
public class UserRoleEntity implements UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    public void setId(Long id) {
        this.id = id;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleEntity that = (UserRoleEntity) o;
        return id != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
