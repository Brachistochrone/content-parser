package com.likhomanov.dao.entities;

import com.likhomanov.model.User;
import com.likhomanov.model.UserRole;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
public class UserEntity implements User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NaturalId
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "login")
    private String login;
    @OneToMany( mappedBy = "user",
                fetch = FetchType.EAGER,
                cascade = CascadeType.ALL,
                orphanRemoval = true)
    private Set<UserRoleEntity> roles = new HashSet<>();

    public void setId(Long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRoles(Set<UserRoleEntity> roles) {
        this.roles = roles;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public Set<? extends UserRole> getRoles() {
        return roles;
    }

    public void addRole(UserRoleEntity role) {
        role.setUser(this);
        roles.add(role);
    }

    public void removeRole(UserRoleEntity role) {
        roles.remove(role);
        role.setUser(null);
    }
}
