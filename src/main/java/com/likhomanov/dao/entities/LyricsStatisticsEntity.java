package com.likhomanov.dao.entities;

import com.likhomanov.enums.Language;
import com.likhomanov.model.LyricsException;
import com.likhomanov.model.LyricsStatistics;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "lyrics_statistics")
public class LyricsStatisticsEntity implements LyricsStatistics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date")
    private Date date;
    @Column(name = "timestamp")
    private Long timestamp;
    @Column(name = "content_url")
    private String url;
    @Column(name = "content")
    private String lyrics;
    @Column(name = "total_number_of_words")
    private Long totalNumberOfWords;
    @Column(name = "number_of_same_words")
    private Long numberOfSameWords;
    @Column(name = "number_of_unique_words")
    private Long numberOfUniqueWords;
    @Column(name = "most_popular_word")
    private String mostPopularWord;
    @Enumerated(EnumType.STRING)
    private Language language;
    @OneToOne(  cascade = CascadeType.ALL,
                orphanRemoval = true)
    @JoinColumn(name = "exception_id")
    private LyricsExceptionEntity exception;

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public void setTotalNumberOfWords(Long totalNumberOfWords) {
        this.totalNumberOfWords = totalNumberOfWords;
    }

    public void setNumberOfSameWords(Long numberOfSameWords) {
        this.numberOfSameWords = numberOfSameWords;
    }

    public void setNumberOfUniqueWords(Long numberOfUniqueWords) {
        this.numberOfUniqueWords = numberOfUniqueWords;
    }

    public void setMostPopularWord(String mostPopularWord) {
        this.mostPopularWord = mostPopularWord;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setLyricsException(LyricsExceptionEntity exception) {
        this.exception = exception;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getLyrics() {
        return lyrics;
    }

    @Override
    public Long getTotalNumberOfWords() {
        return totalNumberOfWords;
    }

    @Override
    public Long getNumberOfSameWords() {
        return numberOfSameWords;
    }

    @Override
    public Long getNumberOfUniqueWords() {
        return numberOfUniqueWords;
    }

    @Override
    public String getMostPopularWord() {
        return mostPopularWord;
    }

    @Override
    public Language getLanguage() {
        return language;
    }

    @Override
    public LyricsException getLyricsException() {
        return exception;
    }
}
