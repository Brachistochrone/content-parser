package com.likhomanov.dao.entities;

import com.likhomanov.model.LyricsException;

import javax.persistence.*;

@Entity
@Table(name = "lyrics_exception")
public class LyricsExceptionEntity implements LyricsException {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "exception_type")
    private String exceptionType;
    @Column(name = "exception_message")
    private String exceptionMessage;

    public void setId(Long id) {
        this.id = id;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getExceptionType() {
        return exceptionType;
    }

    @Override
    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
