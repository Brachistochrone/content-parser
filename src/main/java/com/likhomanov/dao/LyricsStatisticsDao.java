package com.likhomanov.dao;

import com.likhomanov.dao.entities.LyricsStatisticsEntity;
import com.likhomanov.model.LyricsStatistics;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LyricsStatisticsDao extends CrudRepository<LyricsStatisticsEntity, Long> {

    Optional<List<LyricsStatistics>> findAllByUrl(String url);
}
