package com.likhomanov.web.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.exceptions.UserAuthenticationException;
import com.likhomanov.services.mappers.InstanceEntityDtoMapper;
import com.likhomanov.web.dtos.AuthenticatedUserDto;
import com.likhomanov.web.dtos.LoginUserDto;
import com.likhomanov.web.dtos.LoginUserFailedDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final InstanceEntityDtoMapper mapper;
    private final String secret;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager,
                                   InstanceEntityDtoMapper mapper,
                                   String secret) {
        this.authenticationManager = authenticationManager;
        this.mapper = mapper;
        this.secret = secret;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        try (ServletInputStream inStream = request.getInputStream()){
            final LoginUserDto loginUserDto = new ObjectMapper().readValue(inStream, LoginUserDto.class);

            final String email = loginUserDto.getEmail();
            final String password = loginUserDto.getPassword();

            Authentication authentication = new UsernamePasswordAuthenticationToken(email,
                                                                                    password,
                                                                                    Collections.emptyList());
            return authenticationManager.authenticate(authentication);
        } catch (IOException e) {
            throw new UserAuthenticationException("User authentication failed", e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException {
        final Object principal = authResult.getPrincipal();
        if (!(principal instanceof User)) {
            sendStatusMessage(response, INTERNAL_SERVER_ERROR, "User authentication failed");
            throw new UserAuthenticationException("User authentication failed");
        }

        User user = (User) principal;

        AuthenticatedUserDto authenticatedUser = mapper.springUserToAuthenticatedUser(user);

        final String subject = new ObjectMapper().writeValueAsString(authenticatedUser);
        final Date expiresAt = new Date(System.currentTimeMillis() + 300_000);
        final Algorithm algorithm = Algorithm.HMAC512(secret);
        final String token = JWT.create().withSubject(subject).withExpiresAt(expiresAt).sign(algorithm);

        response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION);
        response.addHeader(HttpHeaders.AUTHORIZATION, token);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException {
        sendStatusMessage(response, FORBIDDEN, failed.getMessage());
    }

    private void sendStatusMessage(HttpServletResponse response,
                                   HttpStatus code,
                                   String message) throws IOException {
        LoginUserFailedDto failedDto = new LoginUserFailedDto();
        failedDto.setTimestamp(Calendar.getInstance().getTime().toString());
        failedDto.setStatus(code.value());
        failedDto.setError(code.getReasonPhrase());
        failedDto.setMessage(message);
        failedDto.setPath("/content-parser-app/");

        final String jsonBody = new ObjectMapper().writeValueAsString(failedDto);

        PrintWriter writer = response.getWriter();
        response.setStatus(code.value());
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        writer.print(jsonBody);
        writer.flush();
    }
}
