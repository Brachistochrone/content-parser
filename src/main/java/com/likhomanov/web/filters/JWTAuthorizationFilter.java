package com.likhomanov.web.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likhomanov.exceptions.UserAuthorizationException;
import com.likhomanov.services.mappers.InstanceEntityDtoMapper;
import com.likhomanov.web.dtos.AuthenticatedUserDto;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final String secret;
    private final InstanceEntityDtoMapper mapper;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager,
                                  InstanceEntityDtoMapper mapper,
                                  String secret) {
        super(authenticationManager);
        this.mapper = mapper;
        this.secret = secret;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (token != null) {
            Authentication authentication = decipherToken(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }

    private Authentication decipherToken(@NotNull String token) throws UserAuthorizationException {
        String subject = JWT.require(Algorithm.HMAC512(secret))
                            .build()
                            .verify(token)
                            .getSubject();
        try {

            AuthenticatedUserDto authenticatedUser = new ObjectMapper().readValue(subject, AuthenticatedUserDto.class);

            if (authenticatedUser == null) return null;

            return new UsernamePasswordAuthenticationToken(authenticatedUser,
                                                           null,
                                                           extractAuthorities(authenticatedUser));
        } catch (JsonProcessingException e) {
            throw new UserAuthorizationException("User authorization failed", e);
        }
    }

    private Collection<GrantedAuthority> extractAuthorities(AuthenticatedUserDto authenticatedUser) {

        return authenticatedUser.getRoles()
                                .stream()
                                .map(mapper::roleToAuthority)
                                .collect(Collectors.toList());
    }
}
