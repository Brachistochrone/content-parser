package com.likhomanov.web.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {

    @JsonProperty("user_id")
    private Long id;
    @JsonProperty("user_email")
    private String email;
    @JsonProperty("user_login")
    private String login;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
