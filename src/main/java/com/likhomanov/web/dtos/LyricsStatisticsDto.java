package com.likhomanov.web.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.likhomanov.enums.Language;

public class LyricsStatisticsDto {

    @JsonProperty("stat_id")
    private Long id;
    @JsonProperty("content_url")
    private String url;
    @JsonProperty("content")
    private String content;
    @JsonProperty("total_number_of_words")
    private Long totalNumberOfWords;
    @JsonProperty("number_of_same_words")
    private Long numberOfSameWords;
    @JsonProperty("number_of_unique_words")
    private Long numberOfUniqueWords;
    @JsonProperty("most_popular_word")
    private String mostPopularWord;
    @JsonProperty("language")
    private Language language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getTotalNumberOfWords() {
        return totalNumberOfWords;
    }

    public void setTotalNumberOfWords(Long totalNumberOfWords) {
        this.totalNumberOfWords = totalNumberOfWords;
    }

    public Long getNumberOfSameWords() {
        return numberOfSameWords;
    }

    public void setNumberOfSameWords(Long numberOfSameWords) {
        this.numberOfSameWords = numberOfSameWords;
    }

    public Long getNumberOfUniqueWords() {
        return numberOfUniqueWords;
    }

    public void setNumberOfUniqueWords(Long numberOfUniqueWords) {
        this.numberOfUniqueWords = numberOfUniqueWords;
    }

    public String getMostPopularWord() {
        return mostPopularWord;
    }

    public void setMostPopularWord(String mostPopularWord) {
        this.mostPopularWord = mostPopularWord;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
