package com.likhomanov.web.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.likhomanov.enums.Role;

import java.util.Set;

public class AuthenticatedUserDto {

    @JsonProperty("user_email")
    private String email;
    @JsonProperty("user_password")
    private String password;
    @JsonProperty("user_roles")
    private Set<Role> roles;
    @JsonProperty("user_account_non_expired")
    private boolean accountNonExpired;
    @JsonProperty("user_account_non_locked")
    private boolean accountNonLocked;
    @JsonProperty("user_credentials_non_expired")
    private boolean credentialsNonExpired;
    @JsonProperty("user_enabled")
    private boolean enabled;

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
