package com.likhomanov.web.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class LyricsStatisticsListDto {

    @JsonProperty("urls_count")
    private Integer urlsCount;
    @JsonProperty("lyrics_statistics")
    private List<LyricsStatisticsDto> statisticsDtos = new ArrayList<>();

    public Integer getUrlsCount() {
        return urlsCount;
    }

    public void setUrlsCount(Integer urlsCount) {
        this.urlsCount = urlsCount;
    }

    public List<LyricsStatisticsDto> getStatisticsDtos() {
        return statisticsDtos;
    }

    public void setStatisticsDtos(List<LyricsStatisticsDto> statisticsDtos) {
        this.statisticsDtos = statisticsDtos;
    }

    public void addStatisticsDto(LyricsStatisticsDto dto) {
        statisticsDtos.add(dto);
    }
}
