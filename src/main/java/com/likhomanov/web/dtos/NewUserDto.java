package com.likhomanov.web.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewUserDto extends UserDto {

    @JsonProperty("user_password")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
