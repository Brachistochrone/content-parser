package com.likhomanov.web.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UserListDto {

    @JsonProperty("users_count")
    private Integer usersCount;
    @JsonProperty("users")
    private List<UserDto> users;

    public Integer getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(Integer usersCount) {
        this.usersCount = usersCount;
    }

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }
}
