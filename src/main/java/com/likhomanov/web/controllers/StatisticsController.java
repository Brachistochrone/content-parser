package com.likhomanov.web.controllers;

import com.likhomanov.services.StatisticsService;
import com.likhomanov.web.dtos.LyricsStatisticsDto;
import com.likhomanov.web.dtos.UrlsListDto;
import com.likhomanov.web.dtos.LyricsStatisticsListDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;

    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public LyricsStatisticsListDto analyzeLyrics(@RequestBody UrlsListDto request) {
        return statisticsService.analyzeLyrics(request);
    }

    @GetMapping(value = "/{stat_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LyricsStatisticsDto getStatisticsById(@PathVariable("stat_id") Long id) {
        return statisticsService.findStatisticsById(id);
    }
}
