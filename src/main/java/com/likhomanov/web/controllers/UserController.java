package com.likhomanov.web.controllers;

import com.likhomanov.services.UserService;
import com.likhomanov.web.dtos.NewUserDto;
import com.likhomanov.web.dtos.UserDto;
import com.likhomanov.web.dtos.UserListDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto createUser(@RequestBody NewUserDto newUser) {
        return userService.saveNewUser(newUser);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public UserListDto getAllUsers() {
        return userService.getAllUsers();
    }
}
