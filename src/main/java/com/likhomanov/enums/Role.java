package com.likhomanov.enums;

public enum Role {
    ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_USER
}
