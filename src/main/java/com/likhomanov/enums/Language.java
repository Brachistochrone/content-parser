package com.likhomanov.enums;

public enum Language {
    ENGLISH, RUSSIAN, OTHER
}
