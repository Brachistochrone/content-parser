package com.likhomanov.model;

import com.likhomanov.enums.Language;

import java.util.Date;

public interface LyricsStatistics {

    Long getId();

    Date getDate();

    Long getTimestamp();

    String getUrl();

    String getLyrics();

    Long getTotalNumberOfWords();

    Long getNumberOfSameWords();

    Long getNumberOfUniqueWords();

    String getMostPopularWord();

    Language getLanguage();

    LyricsException getLyricsException();
}
