package com.likhomanov.model;

import com.likhomanov.enums.Role;

public interface UserRole {

    Long getId();

    Role getRole();

    User getUser();
}
