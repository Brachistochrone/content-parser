package com.likhomanov.model;

import com.likhomanov.enums.Language;

import java.util.Date;

public class LyricsStatisticsInstance implements LyricsStatistics {

    private Long id;
    private Date date;
    private Long timestamp;
    private String url;
    private String lyrics;
    private Long totalNumberOfWords;
    private Long numberOfSameWords;
    private Long numberOfUniqueWords;
    private String mostPopularWord;
    private Language language;
    private LyricsException exception;

    public void setId(Long id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public void setTotalNumberOfWords(Long totalNumberOfWords) {
        this.totalNumberOfWords = totalNumberOfWords;
    }

    public void setNumberOfSameWords(Long numberOfSameWords) {
        this.numberOfSameWords = numberOfSameWords;
    }

    public void setNumberOfUniqueWords(Long numberOfUniqueWords) {
        this.numberOfUniqueWords = numberOfUniqueWords;
    }

    public void setMostPopularWord(String mostPopularWord) {
        this.mostPopularWord = mostPopularWord;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setLyricsException(LyricsException exception) {
        this.exception = exception;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getLyrics() {
        return lyrics;
    }

    @Override
    public Long getTotalNumberOfWords() {
        return totalNumberOfWords;
    }

    @Override
    public Long getNumberOfSameWords() {
        return numberOfSameWords;
    }

    @Override
    public Long getNumberOfUniqueWords() {
        return numberOfUniqueWords;
    }

    @Override
    public String getMostPopularWord() {
        return mostPopularWord;
    }

    @Override
    public Language getLanguage() {
        return language;
    }

    @Override
    public LyricsException getLyricsException() {
        return exception;
    }
}
