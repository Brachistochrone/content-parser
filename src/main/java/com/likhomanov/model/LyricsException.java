package com.likhomanov.model;

public interface LyricsException {

    Long getId();

    String getExceptionType();

    String getExceptionMessage();
}
