package com.likhomanov.model;

public class LyricsExceptionInstance implements LyricsException {

    private Long id;
    private String exceptionType;
    private String exceptionMessage;

    public void setId(Long id) {
        this.id = id;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getExceptionType() {
        return exceptionType;
    }

    @Override
    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
