package com.likhomanov.model;

public class Word implements Comparable<Word>{

    private final String word;
    private Integer quantity = 0;

    public Word(String word) {
        this.word = word;
        quantity++;
    }

    public void count() {
        quantity++;
    }

    public String getWord() {
        return word;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public int compareTo(Word o) {
        return quantity.compareTo(o.quantity);
    }
}
