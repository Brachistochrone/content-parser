package com.likhomanov.model;

import java.util.HashSet;
import java.util.Set;

public class UserInstance implements User {

    private Long id;
    private String email;
    private String password;
    private String login;
    private Set<UserRoleInstance> roles = new HashSet<>();

    public void setId(Long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRoles(Set<UserRoleInstance> roles) {
        this.roles = roles;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public Set<? extends UserRole> getRoles() {
        return roles;
    }

    public void addRole(UserRoleInstance role) {
        role.setUser(this);
        roles.add(role);
    }

    public void removeRole(UserRoleInstance role) {
        roles.remove(role);
        role.setUser(null);
    }
}
