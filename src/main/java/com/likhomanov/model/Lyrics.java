package com.likhomanov.model;

import java.util.List;

public class Lyrics {

    private final String url;
    private final String title;
    private final List<String> lines;

    public Lyrics(String url, String title, List<String> lines) {
        this.url = url;
        this.title = title;
        this.lines = lines;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getLines() {
        return lines;
    }
}
