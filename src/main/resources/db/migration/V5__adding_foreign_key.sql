alter table lyrics_statistics
add column exception_id INT NULL;

alter table lyrics_statistics
add foreign key (exception_id) references lyrics_exception (id);