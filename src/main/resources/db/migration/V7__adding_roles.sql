CREATE TABLE IF NOT EXISTS user_roles
(
    id                          SERIAL PRIMARY KEY,
    role                        VARCHAR(256) NOT NULL,
    user_id                     BIGINT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);