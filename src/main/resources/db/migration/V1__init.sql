CREATE TABLE IF NOT EXISTS lyrics_statistics
(
    id                          SERIAL PRIMARY KEY,
    "date"                      DATE,
    "timestamp"                 TIMESTAMP,
    content_url                 VARCHAR(256),
    "content"                   TEXT,
    total_number_of_words       INT,
    number_of_same_words        INT,
    number_of_unique_words      INT,
    most_popular_word           VARCHAR(50)
);