alter table lyrics_statistics
drop  column "timestamp";

alter table lyrics_statistics
add  column "timestamp" BIGINT NULL;