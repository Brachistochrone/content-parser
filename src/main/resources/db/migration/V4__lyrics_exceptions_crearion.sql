CREATE TABLE IF NOT EXISTS lyrics_exception
(
    id                          SERIAL PRIMARY KEY,
    exception_type              VARCHAR(256),
    exception_message           VARCHAR(256)
);