insert into users (email, password, login)
values ('superadmin@gmail.com', '$2a$10$GXsJFNA/dDwJrmQC7JcxXe3fZupJDI/3lhz3IQeIFqhympZIfhAMW', 'Admin');

with super_admin_id as (select currval('users_id_seq'))
insert into user_roles (role, user_id)
values ('SUPER_ADMIN', (select * from super_admin_id));